package single;
import java.io.*;
import java.util.Scanner;

public class singlethreaded {
    public static boolean checkifPrime(int num){
        if (num <= 1) return false;
        for (int i = 2; i<= num/2; i++) {
            if (num % i == 0){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) throws Exception {
        // Prints "Hello, World" to the terminal window.
        long startTime = System.nanoTime();
        Scanner scanner = new Scanner(new File("/home/alex/Documents/uni/concurrency_and_multithreading/prime multithreaded/input.txt"));
        int n = scanner.nextInt();
        scanner.close();
        System.out.printf("Hello, World! n is %d \n", n);
        for (int i = 2; i < n; i++) {
            if (checkifPrime(i)){
                System.out.printf("%d ", i);
            }
        }
        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.printf("\n totalTime is: %dms\n", totalTime / 1000000);

    }

}
