package multi;
import java.io.*;
import java.util.Scanner;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit ;

public class multithreaded {
    public static void main(String[] args) throws Exception {
        // Prints "Hello, World" to the terminal window.
        long startTime = System.nanoTime();
        Scanner scanner = new Scanner(new File("/home/alex/Documents/uni/concurrency_and_multithreading/prime multithreaded/input.txt"));
        int n = scanner.nextInt();
        int range = scanner.nextInt();
        scanner.close();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        System.out.printf("Hello, World! n is %d \n", n);
        for (int i = 2; i < n; i+=range) {
            threadclass task = new threadclass(i, range);
            executor.execute(task);
        }
        executor.shutdown();
        if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
            executor.shutdownNow();
        }
        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.printf("\n totalTime is: %dms\n", totalTime / 1000000);
    }

}
