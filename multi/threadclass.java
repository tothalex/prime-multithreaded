package multi;
// Java code for thread creation by extending 
// the Thread class 

// implement runnable instead of extending thread
class threadclass extends Thread 
{ 
    private int num, range;

    public threadclass(int num, int range) {
        this.num = num;
        this.range = range;
    }

    public static boolean checkifPrime(int num){
        if (num <= 1) return false;
        for (int i = 2; i<= num/2; i++) {
            if (num % i == 0){
                return false;
            }
        }
        return true;
    }


    public void run() 
    { 
        try
        { 
            for (int i = this.num; i < this.num + this.range; i++) {
                if (checkifPrime(i)){
                    System.out.printf("%d ", i);
                }
            }
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught"); 
        } 
    } 
} 
  